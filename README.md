# Requirements #

This is a case management application.

A case is a set of tasks that are dependent on each other.

A case is considered to be closed when all its tasks are completed.

Each case has an overall owner who's responsible for the completion.

Users can be Case Managers, Operators, or both.

Only case managers are allowed to create a case. All the tasks immediately subsequent to "Start" will be assigned to the case manager who created the case, e.g. when user CM1 creates a case, the task "Enter customer name and email address" is assigned to CM1.

Only the users assigned to a task are allowed to complete the task.

A task can only be completed once the preceding tasks are completed, e.g. "Enter customer address" can not be completed until "Enter customer country" has been completed.

Once a task is completed, all immediately subsequent tasks are assigned to the same user that completed the task, e.g. Once "Enter customer name and email address" is completed by UserX, the "Enter customer country" task is assigned to UserX.

A user can re-assign incomplete tasks assigned to them, e.g. if task "Enter customer country" is assigned to UserX then UserX can assign "Enter customer address" to UserY and UserZ, complete "Enter customer telephone number" themselves and assign "Enter customer web page" to UserZ.

**The web application will:**

1. Log in users based on a hard-coded list of user/password pairs, i.e. no need to store data in an external data source. 
2. Associate roles to users using a hard-coded list of user/roles pairs. 
3. Support a single type of case, as represented by the following BPM diagram:

4. Show in the main page 
	A. list of all the cases assigned to the logged in user, if they are a Case Manager. Complete and incomplete cases must be visually different, e.g. red vs green. 
	B. A list of all the incomplete tasks assigned to them with their corresponding case, If they are an Operator. 
	C. Both lists if the user is both a Case Manager and an Operator.
	
### Likely changes in the future ###

1. The hard-coded list of user/password pairs will most likely be replaced with a OAuth 2.0 workflow that will integrate with Google, Facebook and Microsoft accounts. 
2. The hard-coded list of roles will most likely have to be maintained by an admin through a user interface.